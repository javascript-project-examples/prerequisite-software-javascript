# Prerequisite Software - JavaScript

The software listed below should be installed on your computer before starting the JavaScript portion of your full-stack course.

## Git

Download Link: [git-scm.com/](https://git-scm.com/)

Git will be used to submit all your assignments. 

## NodeJS 

#### Source

Download Link: [nodejs.org](https://nodejs.org/en/)

Install the **LTS** version of NodeJS. The NodeJS installation includes `npm`

## Code Editor

### Visual Studio Code

Download Link: [code.visualstudio.com](https://code.visualstudio.com/)

Visual Studio Code is an extensible text editor by Microsoft.

### Jetbrains' IDE

Redeem code will be provided

## Postman API Testing

Download Link: [postman.com/](https://www.postman.com/downloads/)

Postman is a poweful tool to debug and test API endpoints.

## Heroku CLI

Download Link: [heroku.com](https://devcenter.heroku.com/articles/heroku-cli#download-and-install)

Heroku is used to deploy your JavaScript assignments. 

## Figma 

Download Link: [https://www.figma.com/downloads/](https://www.figma.com/downloads/)

Figma is used for prototyping and planning components. 

## Browser Dev Tools

#### Angular Devtools:
- Chrome, Edge, Opera, Brave: [Angluar Developer Tools] - Chrome Web Store (google.com)[https://chrome.google.com/webstore/detail/angular-devtools/ienfalfjdbdpebioblfackkekamfmbnh]
- Firefox: NOT AVAILABLE
#### React Devtools:
- Chrome, Edge, Opera, Brave: [React Developer Tools - Chrome Web Store (google.com)](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi?hl=en)
- Firefox: [React Developer Tools – Get this Extension for 🦊 Firefox (en-US) (mozilla.org)](https://addons.mozilla.org/en-US/firefox/addon/react-devtools/)
#### Redux Devtools:
- Chrome, Edge, Opera, Brave: [Redux DevTools - Chrome Web Store (google.com)](https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd?hl=en)
- Firefox: [Redux DevTools – Get this Extension for 🦊 Firefox (en-US) (mozilla.org)](https://addons.mozilla.org/en-US/firefox/addon/reduxdevtools/)
#### VueJS Devtools
- Chrome, Edge, Opera, Brave: [Vue.js devtools - Chrome Web Store (google.com)](https://chrome.google.com/webstore/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbpd?hl=en)
- Firefox: [Vue.js devtools – Get this Extension for 🦊 Firefox (en-US) (mozilla.org)](https://addons.mozilla.org/en-US/firefox/addon/vue-js-devtools/)
